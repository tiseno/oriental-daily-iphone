//
//  AppDelegate.m
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/8/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

NSString *const FBSessionStateChangedNotification =
@"com.example.Login:FBSessionStateChangedNotification";

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize isclick;
@synthesize tokendevice;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    NSDictionary *tmpDic = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    
    if (tmpDic != nil) {
        if( [[tmpDic objectForKey:@"aps"] objectForKey:@"news_id"] != NULL)
            news_id = [[tmpDic objectForKey:@"aps"] objectForKey:@"news_id"];

        newsid = [news_id intValue];
        
        Main_PushNotificationNews *GtestClasssViewController=[[[Main_PushNotificationNews alloc] initWithNibName:@"Main_PushNotificationNews"  bundle:nil] autorelease];
        GtestClasssViewController.newsid = newsid;
        GtestClasssViewController.whichpage = 2;
        GtestClasssViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.window.rootViewController = GtestClasssViewController;
        
    }else{
        self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
        self.window.rootViewController = self.viewController;
    }
    
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    [self.window makeKeyAndVisible];
    self.isclick = NO;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (FBSession.activeSession.state == FBSessionStateCreatedOpening) {
        [FBSession.activeSession close]; // so we close our session and start over
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *temp = [NSString stringWithFormat:@"%@", deviceToken];
    temp = [temp stringByReplacingOccurrencesOfString:@"<" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@" " withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@">" withString:@""];
    self.tokendevice = temp;
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive)
    {
        NSString *badge;
        NSString *sound;
        
        if( [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] != NULL)
        {
            alertMsg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        }
        
        if( [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] != NULL)
        {
            badge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"];
        }
        
        if( [[userInfo objectForKey:@"aps"] objectForKey:@"sound"] != NULL)
        {
            sound = [[userInfo objectForKey:@"aps"] objectForKey:@"sound"];
        }
        
        if( [[userInfo objectForKey:@"aps"] objectForKey:@"news_id"] != NULL)
            news_id = [[userInfo objectForKey:@"aps"] objectForKey:@"news_id"];
        
        newsid = [news_id intValue];
        
        AudioServicesPlayAlertSound (1007);
        
        alert = [[UIAlertView alloc] initWithTitle:alertMsg message:@"" delegate:self cancelButtonTitle:@"退出" otherButtonTitles:@"閱讀", nil];
        [alert show];
        [alert release];
    }else{
        if( [[userInfo objectForKey:@"aps"] objectForKey:@"news_id"] != NULL)
            news_id = [[userInfo objectForKey:@"aps"] objectForKey:@"news_id"];

        newsid = [news_id intValue];
        
        for(UIView *uiview in self.window.subviews)
            [uiview removeFromSuperview];
        
        Main_PushNotificationNews *GtestClasssViewController=[[[Main_PushNotificationNews alloc] initWithNibName:@"Main_PushNotificationNews"  bundle:nil] autorelease];
        GtestClasssViewController.newsid = newsid;
        GtestClasssViewController.whichpage = 1;
        GtestClasssViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.window.rootViewController = GtestClasssViewController;
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }else{
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        for(UIView *uiview in self.window.subviews)
            [uiview removeFromSuperview];
        Main_PushNotificationNews *GtestClasssViewController=[[[Main_PushNotificationNews alloc] initWithNibName:@"Main_PushNotificationNews"  bundle:nil] autorelease];
        GtestClasssViewController.newsid = newsid;
        GtestClasssViewController.whichpage = 1;
        GtestClasssViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        self.window.rootViewController = GtestClasssViewController;
    }
}


- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error)
                self.isclick = YES;
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:FBSessionStateChangedNotification
     object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_likes",
                            @"read_stream", @"publish_actions",
                            nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session,
                                                         FBSessionState state,
                                                         NSError *error) {
                                         [self sessionStateChanged:session
                                                             state:state
                                                             error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}

- (void) closeSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        NSLog(@"logged in");
    } else {
        NSLog(@"logged out");
    }
}

@end
