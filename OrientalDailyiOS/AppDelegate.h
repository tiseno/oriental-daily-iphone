//
//  AppDelegate.h
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/8/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FacebookSDK/FacebookSDK.h>
#import <AudioToolbox/AudioToolbox.h>
#import "Facebook.h"
#import "Main_NewsDetail.h"
#import "Main_PushNotificationNews.h"

extern NSString *const FBSessionStateChangedNotification;

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>{
    UIAlertView *alert;
    NSString *alertMsg, *news_id;
    int newsid;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (nonatomic) BOOL isclick;
@property (strong, nonatomic) NSString *tokendevice;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void) closeSession;

@end
