//
//  ViewController.m
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/8/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self performSelector:@selector(latest) withObject:nil afterDelay:1];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation==UIInterfaceOrientationPortrait)
        return YES;
    else
        return NO;
}

- (void) latest
{
    DatabaseAction *getsurvey = [[[DatabaseAction alloc] init] autorelease];
    int check = [getsurvey retrieveSurvey];
    if(check == 1){
        Main_AllLatestNews *GtestClasssViewController=[[[Main_AllLatestNews alloc] initWithNibName:@"Main_AllLatestNews"  bundle:nil] autorelease];
        GtestClasssViewController.autodownload = YES;
        [self presentModalViewController:GtestClasssViewController animated:NO];
    }else{
        Menu_newUserForm *GtestClasssViewController=[[[Menu_newUserForm alloc] initWithNibName:@"Menu_newUserForm"  bundle:nil] autorelease];
        [self presentModalViewController:GtestClasssViewController animated:NO];
    }
    
    /*Main_PushNotificationNews *GtestClasssViewController=[[[Main_PushNotificationNews alloc] initWithNibName:@"Main_PushNotificationNews"  bundle:nil] autorelease];
    GtestClasssViewController.newsid = 29342;
    [self presentModalViewController:GtestClasssViewController animated:NO];*/
}

- (void)dealloc
{
    [super dealloc];
}
@end
